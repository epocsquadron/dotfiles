#!/usr/bin/env sh
# Shell commands to run when GNOME Night Theme Switcher switches to day
set -e

# Update Helix Editor config to use light theme
sed -i 's/theme = ".*"/theme = "catppuccin_latte"/' ${HOME}/.config/helix/config.toml
pkill -USR1 hx
