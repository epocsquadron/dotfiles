#!/usr/bin/env sh
# Shell commands to run when GNOME Night Theme Switcher switches to night
set -e

# Update Helix Editor config to use dark theme
sed -i 's/theme = ".*"/theme = "catppuccin_macchiato"/' ${HOME}/.config/helix/config.toml
pkill -USR1 hx
