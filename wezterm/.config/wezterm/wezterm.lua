local wezterm = require 'wezterm';

wezterm.on("format-tab-title", function(tab, tabs, panes, config, hover, max_width)
  -- ensure that the titles fit in the available space,
  -- and that we have room for the edges
  return wezterm.truncate_left(tab.active_pane.title, 30)
end)

function scheme_for_appearance(appearance)
  if appearance:find 'Dark' then
    return 'Azu (Gogh)'
  else
    return 'CLRS'
  end
end


return {
  window_decorations = "RESIZE",
  
  color_scheme = scheme_for_appearance(wezterm.gui.get_appearance()),

  -- enable_wayland = true,

  -- Font with programming ligatures
  font = wezterm.font("Rec Mono Linear"),
  font_rules= {
    {
      italic = true,
      font = wezterm.font("Rec Mono Semicasual", {italic=true}),
    },

    {
      italic = true,
      intensity = "Bold",
      font = wezterm.font("Rec Mono Semicasual", {italic=true, bold=true}),
    },

    -- This is automatically derived from the main font, so no need to be
    -- explicit about it.
    -- {
    --   intensity = "Bold",
    --   font = wezterm.font({"Rec Mono Linear"}, {bold=true}),
    -- },

    {
      intensity = "Half",
      font = wezterm.font("Rec Mono Duotone"),
    },
  },
  font_size = 11.5,

  -- Needed with Rec Mono to have enough room for undercurl et al
  line_height = 1.05,
  underline_position = "-1pt",

  window_padding = {
    left = 0,
    right = 0,
    top = 0,
    bottom = 0,
  },

  quick_select_patterns = {
    "feature/[A-Za-z0-9-_]+",
  },

  -- Managing this through AUR, so no update notifications please
  check_for_updates = false,

  -- Use the dedicated terminfo so we get all the newest goodies,
  -- like fancy underline escapes
  term = "wezterm",

  -- Fancy terminal escapes so we can map Ctrl-J unambiguously
  -- in programs like kakoune
  enable_csi_u_key_encoding = true,

  -- I've got the RAM, let's keep some history for when I need it
  scrollback_lines = 15000,

  -- don't leave us with useless terminals when zsh exits badly
  skip_close_confirmation_for_processes_named = {
    "bash", "sh", "zsh", "fish", "tmux", "ion", "nu"
  },

  keys = {
    {key="x", mods="CTRL|SHIFT", action=wezterm.action{CloseCurrentPane={confirm=true}}},
    {key="y", mods="CTRL|SHIFT", action="ActivateCopyMode"},
    {key="z", mods="CTRL|SHIFT", action="TogglePaneZoomState"},
    {key="|", mods="CTRL|SHIFT", action=wezterm.action{SplitHorizontal={domain="CurrentPaneDomain"}}},
    {key="-", mods="CTRL", action=wezterm.action{SplitVertical={domain="CurrentPaneDomain"}}},
  },
  
  -- debug_key_events = true,
}
