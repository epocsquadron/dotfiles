# Epocsquadron's Dotfiles, via GNU Stow

## Why Stow

It provides a very simple interface to do exactly what you would want to -- symlink from a central repo into your home directory, organized by program name. No need for wrappers or anything like that. KISS.

## Installation

* Install GNU Stow
* Clone to a directory inside your home directory (I use `~/.stowfiles`)
* Change directory to the new directory
* Run `stow` with a list of the programs you want to use config for (eg `stow tmux zsh` for only dotfiles related to those programs)
    * If you have dotfiles already in place for those, you'll want to move them out of the way
    * Alternatively, you can absorb the existing file into your cpoy of the repo with `stow --adopt`

### zsh

If you're using the zsh config you'll need [alien](https://github.com/eendroroy/alien) installed to `~/.config/zsh/alien` first.

### lf

If you're using the lf config you'll need `tar`, `unzip`, `unrar`, `mdcat`, and `bat` installed.

### kakoune

If you use the kak config, you'll need to install plug.kak at `~/.config/kak/plugins` and then use it from within kakoune to install the plugins included in `kak/.config/kak/kakrc` in order for it to work fully. Yes, kakoune will still run without them, it'll just throw errors into the `*debug*` buffer.
